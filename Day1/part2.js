const { readFileToArray } = require('../utils');

const getTotals = () => {
    const values = readFileToArray('Day1/input');
    const totals = [];
    let currentTotal = 0;
    for (let i = 0; i < values.length; i++) {
        if (i === values.length - 1) totals.push(currentTotal);
        else if (values[i].trim() === '') {
            totals.push(currentTotal);
            currentTotal = 0;
            continue;
        }
        currentTotal += parseInt(values[i]);;
    }
    return totals;
}

const sortedTotals = totals => totals.sort((a, b) => b - a);

const answer = () => {
   const totals = sortedTotals(getTotals());
   return totals[0] + totals[1] + totals[2];
};

module.exports = {
    getTotals,
    sortedTotals,
    answer
};