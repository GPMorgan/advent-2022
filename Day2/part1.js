const rock = ['A', 'X'];
const paper = ['B', 'Y'];
const scissors = ['C', 'Z'];

const scores = {
    [rock[0]]: 1,
    [paper[0]]: 2,
    [scissors[0]]: 3,
    [rock[1]]: 1,
    [paper[1]]: 2,
    [scissors[1]]: 3
};

const win = (player1, player2) => 
       player1 === rock[0] && player2 === paper[1]
    || player1 === paper[0] && player2 === scissors[1]
    || player1 === scissors[0] && player2 === rock[1]
;

const answer = pairs => {
    let score = 0;
    
    pairs.forEach(pair => {
       const [ player1, player2 ] = pair.split(' ');
       score += scores[player2];
       if (scores[player1] === scores[player2]) score += 3;
       else if (win(player1, player2)) return score += 6;
    });
    
    return score;
}

module.exports = {
    rock,
    paper,
    scissors,
    win,
    answer
};