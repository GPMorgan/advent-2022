const ascii_A = 65;
const ascii_a = 97;
const lowerCaseStartingIndexOffset = 1;
const upperCaseStartingIndexOffset = 27;

const scoreForLetter = letter => {
    const characterCode = letter.charCodeAt(0);
    return letter === letter.toUpperCase()
    ? characterCode - ascii_A + upperCaseStartingIndexOffset 
    : characterCode - ascii_a + lowerCaseStartingIndexOffset;
};

const groupSize = 3;

// O(n)
// const answer = input => {
    
//     // get next 3 values
//     // get shortest one
//     // iterate through each character to see if in both other arrays
    
//     let score = 0;
    
//     let itemSet = 1;
//     let map = {};
//     let index = 0;
//     input.forEach(values => {
//         console.log('index', index);
//         const length = values.length;
//         for (let i = 0; i < length; i++) {
//             const value = values[i];          
//             if (!map[value] && itemSet === 1) {
//                 map[value] = 1
//             } else if (map[value]) {
//                 map[value]++; // problem is here
//                 if (map[value] > itemSet) map[value] = itemSet;
//                 if (map[value] === groupSize) {
//                     score += scoreForLetter(value);
//                     console.log(scoreForLetter(value), value, score);
//                     map = {};
//                     itemSet = 0;
//                     break;
//                 }
//             }
//         }
//         itemSet++;
//         index++;
//     })
//     return score;
// }

// O(n^2)
const answer = input => {
    let score = 0;
    
    for (let i = 0; i < input.length; i+=3) {
        const v1 = input[i];
        const v2 = input[i+1];
        const v3 = input[i+2];
        
        for (let j = 0; j < v1.length; j++) {
            const letter = v1[j];
            if(v2.includes(letter) && v3.includes(letter)) {
                score += scoreForLetter(letter);
                break;
            }
        }
    }
    
    return score;
}

module.exports = {
    scoreForLetter,
    answer
};