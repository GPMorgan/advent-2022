const rock = 'A';
const paper = 'B';
const scissors = 'C';

const lose = 'X';
const draw = 'Y';
const win = 'Z';

const scores = {
    [rock]: 1,
    [paper]: 2,
    [scissors]: 3,
    [lose]: 0,
    [draw]: 3,
    [win]: 6
};

const outcomes = {
    [rock] : {
        [lose]: scores[scissors],
        [draw]: scores[rock] + scores[draw],
        [win]: scores[paper] + scores[win]
    },
    [paper] : {
        [lose]: scores[rock],
        [draw]: scores[paper] + scores[draw],
        [win]: scores[scissors] + scores[win]
    },
    [scissors]: {
        [lose]: scores[paper],
        [draw]: scores[scissors] + scores[draw],
        [win]: scores[rock] + scores[win]
    }
}

const answer = pairs => {
    let score = 0;
    
    pairs.forEach(pair => {
       const [ player1, outcome ] = pair.split(' ');
       score += outcomes[player1][outcome];
    });
    
    return score;
}

module.exports = {
    rock,
    paper,
    scissors,
    win, lose, draw,
    outcomes,
    answer
};