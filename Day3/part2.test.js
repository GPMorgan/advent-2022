const { readFileToArray } = require('../utils');
const { scoreForLetter, answer } = require('./part2');

describe('Day 3 - Part 2', () => {    
    describe('answer', () => {   
        const r = 18;
        const Z = 52;
        test('it gets the first common letter in the first given example', () => {
            const input = [
                'vJrwpWtwJgWrhcsFMMfFFhFp',
                'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
                'PmmdzqPrVvPwwTWBwg'
            ];
            expect(answer(input)).toBe(r);
        });
        
        test('it gets the first common letter in the second given example', () => {
            const input = [
                'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
                'ttgJtRGJQctTZtZT',
                'CrZsJsPPZsGzwwsLwLmpwMDw'
            ];
            expect(answer(input)).toBe(Z);
        });
        
        test('it gets the combined score of the example groups', () => {
            const input = [
                'vJrwpWtwJgWrhcsFMMfFFhFp',
                'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
                'PmmdzqPrVvPwwTWBwg',
                'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
                'ttgJtRGJQctTZtZT',
                'CrZsJsPPZsGzwwsLwLmpwMDw'
            ];
            expect(answer(input)).toBe(r + Z);
        });
        
        test('it gets the right score when multiple instances of the same letter are include in some of the strings', () => {
            const input = [
                'JHbQtHVHHLLbTJmmZddgdgwhllMNhhhTgg',
                'spqpNGDjDPMhCFChMj',
                'DBSDDGnpSDsDWqWczcvSqWvsBtJJLLZrRVZLJRbBZNVrBHrV',
            ];
            expect(answer(input)).toBe(scoreForLetter('N'));
        });
        
        test('it gets the final answer', () => {
            const input = readFileToArray('Day3/input');
            expect(answer(input)).toBe(2633);
        });
    });
});