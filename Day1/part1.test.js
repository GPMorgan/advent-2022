const part1 = require('./part1');

describe('Day 1 - Part 1', () => {
    test('it gets the largest total', () => {
        expect(part1()).toBe(73211);
    });
});