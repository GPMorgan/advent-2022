const { readFileToArray } = require('../utils');
const { answer, getGroups } = require('./part2');

describe('Day 4 - Part 2', () => {

    describe('getGroups', () => {   
        
        test('it gets the final answer', () => {
            const input = ['57-93,9-57'];
            const { first, second } = getGroups(input)[0];
            expect(first[0]).toBe(57);
            expect(first[1]).toBe(93);
            expect(second[0]).toBe(9)
            expect(second[1]).toBe(57);
        });
    });
    
    describe('answer', () => {      
       
        test('it counts when the start numbers are equal', () => {
            const input = ['1-3,1-5'];
            expect(answer(input)).toBe(1);
        });
        
        test('it counts when the end numbers are equal', () => {
            const input = ['1-5,3-5'];
            expect(answer(input)).toBe(1);
        });
        
        test('it counts when the first start and second end values are equal', () => {
            const input = ['3-5,1-3'];
            expect(answer(input)).toBe(1);
        });
        
        test('it does counts when the first end and second start values are equal', () => {
            const input = ['3-5,5-7'];
            expect(answer(input)).toBe(1);
        });
        
        test('it counts when the first end is greater than the second start value', () => {
            const input = ['3-8,4-9'];
            expect(answer(input)).toBe(1);
        });
        
        test('it counts when the first end is greater than the second start value', () => {
            const input = ['3-8,4-7'];
            expect(answer(input)).toBe(1);
        });
        
        test('it counts when the first range fully encompasses the second', () => {
            const input = ['3-8,4-5'];
            expect(answer(input)).toBe(1);
        });
        
        test('it counts when the second range fully encompasses the first', () => {
            const input = ['4-5,3-8'];
            expect(answer(input)).toBe(1);
        });
        
        test('it counts when the second range starts in the middle of the first, and ends before the first', () => {
            const input = ['4-8,3-7'];
            expect(answer(input)).toBe(1);
        });
        
        test('it does not count when the first range is entirely before the second', () => {
            const input = ['1-3,4-7'];
            expect(answer(input)).toBe(0);
        });
        
        test('it does not count when the first range is entirely before the second', () => {
            const input = ['4-7,1-3'];
            expect(answer(input)).toBe(0);
        });
        
        test('it gets the final answer', () => {
            const input = readFileToArray('Day4/input');
            expect(answer(input)).toBe(888);
        });
    }); 
});