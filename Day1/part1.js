const { readFileToArray } = require('../utils');

const answer = () => {
    let overallTotal = 0;
    let currentTotal = 0;
    const values = readFileToArray('day1/input');
    
    for (let i = 0; i < values.length; i++) {
        if (currentTotal > overallTotal) overallTotal = currentTotal;
        if (values[i].trim() === '') {
            currentTotal = 0;
            continue;
        }
        currentTotal += parseInt(values[i]);;
    }
    return overallTotal;
}

module.exports = answer;