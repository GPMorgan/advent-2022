const fs = require("fs");

const readFileToArray = (filename) => {
    const file = fs.readFileSync(filename).toString();
    return file.split("\n");
}

module.exports = { 
    readFileToArray 
};