const part2 = require('./part2');

describe('Day 1 - Part 2', () => {
    
    describe('getTotals', () => {
        test('it gets totals', () => {
            expect(part2.getTotals().length).toBeGreaterThan(0);
        });
        
        test('it includes the largest total', () => {
            expect(part2.getTotals()).toContain(73211);
        });
    });
    
    describe('getSortedTotals', () => {
        test('it places the largest value at the start of the array', () => {
            expect(part2.sortedTotals(part2.getTotals())[0]).toBe(73211);
        });
        
        test('it sorts the values in descending order', () => {
            const sortedTotals = part2.sortedTotals(part2.getTotals());
            expect(sortedTotals[1]).toBeLessThan(sortedTotals[0]);
        });
    });
    
    describe('answer', () => {
        test('it gets an answer greater than the largest total', () => {
            expect(part2.answer()).toBeGreaterThan(73211);
        });
        
        test('it adds up the top 3 totals', () => {
            expect(part2.answer()).toBe(213958);
        });
    });
});