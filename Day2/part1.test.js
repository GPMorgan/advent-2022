const { readFileToArray } = require('../utils');
const { win, answer, rock, paper, scissors } = require('./part1');

describe('Day 2 - Part 1', () => {
    
    describe('win', () => {    
        test('rock breaks scissors', () => {
            expect(win(scissors[0], rock[1])).toBe(true);
        });
        
        test('paper covers rock', () => {
            expect(win(rock[0], paper[1])).toBe(true);
        });
        
        test('scissors custs paper', () => {
            expect(win(paper[0], scissors[1])).toBe(true);
        });
    });
    
    describe('answer', () => {   
        test('draw with rock scores 4 (1 for rock, 3 for draw)', () => {
            expect(answer(['A X'])).toBe(4);
        });
        
        test('it gets the final answer', () => {
            const pairs = readFileToArray('Day2/input');
            expect(answer(pairs)).toBe(9177);
        });
    });
});