const getStacks = input => {
    const stacks = [];
    let current;
    let index = 0;
    
    do {
        current = input[index].concat(" ");
        
        for(let i = 0; i < current.length; i += 4) {
            if (!stacks[i/4]) stacks[i/4] = [];
            const value = current.slice(i, i+3).replaceAll('[', '').replaceAll(']', '').trim();
            if (value.length > 0) stacks[i/4].unshift(value);
        }
        
        index++;   
    } while (input[index][1] !== '1');
    
    return stacks;
}

const getInstructions = input => {
    const instructions = [];
    let index = 10;
    do {
        current = input[index];
        const instruction = current.split(' ')
            .map(part => parseInt(part))    
            .filter(part => Number.isInteger(part));
            
        instructions.push(instruction);
        index++;
    } while (index < input.length);
    return instructions;
};

const moveValues1By1_Part1 = (valuesToMove, startStack, endStack) => 
    valuesToMove.reverse().forEach(value => {
        startStack.pop();
        endStack.push(value);
    });
    
const moveAllValues_Part2 = (valuesToMove, startStack, endStack) => 
    valuesToMove.forEach(value => {
        startStack.pop();
        endStack.push(value);
    });

const answer = (stacks, instructions, part = 1) => {
    //console.log('before: ', stacks.map(stack => stack[stack.length - 1]).join(''));
    instructions.forEach(([amount, start, end]) => {
        const startStack = stacks[start - 1];
        const endStack = stacks[end - 1];
        const valuesToMove = startStack.slice(startStack.length - amount);
        part === 1 
            ? moveValues1By1_Part1(valuesToMove, startStack, endStack) 
            : moveAllValues_Part2(valuesToMove, startStack, endStack);
    });
    // console.log('after: ', stacks.map(stack => stack[stack.length - 1]).join(''));
    return stacks.map(stack => stack[stack.length - 1]).join('');
}

module.exports = {
    getStacks,
    getInstructions,
    answer
};