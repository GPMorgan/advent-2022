const { readFileToArray } = require('../utils');
const { getStacks, getInstructions, answer } = require('./part1_2');

describe('Day 5 - Part 1', () => {

    let input;
    beforeAll(() => {
        input = readFileToArray('Day5/input'); 
    });
    
    describe('stacks', () => {
        test('it gets the stacks', () => {
            const stacks = getStacks(input);
            expect(stacks.length).toBe(9);
            expect(stacks[0][0]).toBe('G');
            expect(stacks[0][6]).toBe('B');
            expect(stacks[1][0]).toBe('Z');
            expect(stacks[1][5]).toBe('P');
            expect(stacks[2][0]).toBe('C');
            expect(stacks[2][7]).toBe('F');
            expect(stacks[3][0]).toBe('H');
            expect(stacks[3][7]).toBe('Q');
            expect(stacks[4][0]).toBe('C');
            expect(stacks[4][6]).toBe('D');
            expect(stacks[5][0]).toBe('R');
            expect(stacks[5][3]).toBe('D');
            expect(stacks[6][0]).toBe('H');
            expect(stacks[6][7]).toBe('Q');
            expect(stacks[7][0]).toBe('P');
            expect(stacks[7][2]).toBe('V');
            expect(stacks[8][0]).toBe('D');
            expect(stacks[8][4]).toBe('J');
        }); 
    });
    
    describe('getInstructions', () => {   
        
        test('it gets the instructions', () => {
            const instructions = getInstructions(input);
            expect(instructions.length).toBe(502);
            expect(instructions[0].length).toBe(3);
            expect(instructions[501].length).toBe(3);
        });
    });
    
    describe('answer', () => {   
        
        test('it moves an item from 1 stack to another', () => {
            const stacks = [
              ['A'],
              ['B']  
            ];
            const instructions = [
              [1,1,2]
            ];
            expect(answer(stacks, instructions)).toBe('A');
        });
        
        test('it moves an item back from 1 stack to another', () => {
            const stacks = [
              ['A'],
              ['B']  
            ];
            const instructions = [
              [1,2,1]
            ];
            expect(answer(stacks, instructions)).toBe('B');
        });
        
        test('it moves more items than there are from 1 stack to another', () => {
            const stacks = [
              ['A'],
              ['B']  
            ];
            const instructions = [
              [3,1,2]
            ];
            expect(answer(stacks, instructions)).toBe('A');
        });
       
        test('it satisfies the given test case', () => {
            const stacks = [
                ['Z', 'N'],
                ['M', 'C', 'D'],
                ['P']
            ];
            const instructions = [
                [1,2,1],
                [3,1,3],
                [2,2,1],
                [1,1,2]
            ];
            expect(answer(stacks, instructions)).toBe('CMZ');
        });
                
        test('it gets the final answer', () => {
            const input = readFileToArray('Day5/input'); 
            const stacks = getStacks(input);
            const instructions = getInstructions(input);
            expect(answer(stacks, instructions)).toBe('WCZTHTMPS');
        });
    });
});

describe('Day 5 - Part 2', () => {
    test('it gets the final answer', () => {
        const input = readFileToArray('Day5/input'); 
        const stacks = getStacks(input);
        const instructions = getInstructions(input);
        expect(answer(stacks, instructions, part = 2)).toBe('BLSGJSDTS');
    });
});