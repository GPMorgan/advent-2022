const { readFileToArray } = require('../utils');
const { answer } = require('./part1');

describe('Day _ - Part 1', () => {
    
    describe('answer', () => {   
        
        test('it gets the final answer', () => {
            const input = '';//readFileToArray('Day_/input');
            expect(answer(input)).toBe(0);
        });
    });
});