const { readFileToArray } = require('../utils');
const { outcomes, answer, rock, paper, scissors, win, lose, draw } = require('./part2');

describe('Day 2 - Part 2', () => {
    
    describe('outcomes', () => {    
        test('rock breaks scissors', () => {
            expect(outcomes[rock][draw]).toBe(4);
        });
    });
    
    describe('answer', () => {   
        test('it gets the final answer', () => {
            const pairs = readFileToArray('Day2/input');
            expect(answer(pairs)).toBe(12111);
        });
    });
});