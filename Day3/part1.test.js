const { readFileToArray } = require('../utils');
const { scoreForLetter, answer } = require('./part1');

describe('Day 3 - Part 1', () => {
    
    describe('scoreForLetter', () => {   
        
        test('it scores 1 for the letter "a"', () => {
            expect(scoreForLetter('a')).toBe(1);
        });
        
        test('it scores 1 for the letter "a"', () => {
            expect(scoreForLetter('z')).toBe(26);
        });
        
        test('it scores 1 for the letter "A"', () => {
            expect(scoreForLetter('A')).toBe(27);
        });
        
        test('it scores 1 for the letter "a"', () => {
            expect(scoreForLetter('Z')).toBe(52);
        });
    });
    
    describe('answer', () => {   
        
        test('it matches "a"', () => {
            expect(answer(['abcCBa'])).toBe(1);
        });
        
        test('it matches "B"', () => {
            expect(answer(['aBcBa'])).toBe(28);
        });
        
        test('it adds up 2 scores', () => {
            expect(answer(['aBcBa', 'xyzJKz'])).toBe(28 + 26);
        });
        
        test('it only adds up duplicates across the half-way split', () => {
            expect(answer(['aabbca'])).toBe(1 + 1);
        });
        
        test('it gets the example answer', () => {
            const input = [
                'vJrwpWtwJgWrhcsFMMfFFhFp',
                'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL',
                'PmmdzqPrVvPwwTWBwg',
                'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn',
                'ttgJtRGJQctTZtZT',
                'CrZsJsPPZsGzwwsLwLmpwMDw',
            ];
            expect(answer(input)).toBe(157);
        });
        
        test('it gets the final answer', () => {
            const input = readFileToArray('Day3/input');
            expect(answer(input)).toBe(7785);
        });
    });
});