const ascii_A = 65;
const ascii_a = 97;
const lowerCaseStartingIndexOffset = 1;
const upperCaseStartingIndexOffset = 27;

const scoreForLetter = letter => {
    const characterCode = letter.charCodeAt(0);
    return letter === letter.toUpperCase()
    ? characterCode - ascii_A + upperCaseStartingIndexOffset 
    : characterCode - ascii_a + lowerCaseStartingIndexOffset;
};

const answer = input => {
    let score = 0;
    
    input.forEach(values => {
        const map = {};
        const length = values.length;
        const halfway = length / 2;
        for (let i = 0; i < length; i++) {
            const value = values[i];
            //console.log('i:', i, map[value]);
            if ((i >= halfway) && map[value]) {
                score += scoreForLetter(value);
                //console.log('score', score, value, scoreForLetter(value));
                break;
            } else if (i < halfway) {
                map[value] = 1;
                //console.log(map);
            }
        }
    })
    return score;
}

module.exports = {
    scoreForLetter,
    answer
};