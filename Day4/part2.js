const getGroups = data => {
    const pairs = data.map(line => { 
        const [firstFull, secondFull] = line.split(',');
        const [start1, end1] = firstFull.split('-');
        const [start2, end2] = secondFull.split('-');
        return { 
            first: [parseInt(start1), parseInt(end1)], 
            second: [parseInt(start2), parseInt(end2)] 
        };
    });
    return pairs;
}

const answer = input => {
    const groups = getGroups(input);
    const overlappingGroups = groups.filter(({ first, second }) => {
    
        const [start1, end1] = first;
        const [start2, end2] = second;
    
        return start1 === start2
            || end1 === end2
            || start1 === end2
            || end1 === start2
            || start1 <= start2 && end1 >= start2 && end2 >= end1
            || start1 <= start2 && end1 >= end2
            || start2 <= start1 && end2 >= start1 && end1 >= end2
            || start2 <= start1 && end2 >= end1;
    });
    
    return overlappingGroups.length;
};

module.exports = {
    getGroups,
    answer
};